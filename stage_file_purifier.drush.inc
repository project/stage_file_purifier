<?php
/**
 * @file
 * Purifies the files of a staging site to decrease disk space utilization.
 */

/**
 * Implements hook_drush_command().
 */
function stage_file_purifier_drush_command() {
  $items = array();

  $items['stage-file-purifier'] = array(
    'callback' => 'stage_file_purifier',
    'description' => dt('Purifies the files of a staging site to decrease disk space utilization.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'path' => dt('Path of the dummy files.'),
    ),
    'required-arguments' => TRUE,
    'aliases' => array('sfp', 'pur'),
    'options' => array(
      'extensions' => 'Comma delimited list of file extensions',
    ),
  );

  return $items;
}

/**
 * Replaces certain file types by a symlink to a dummy of the same type.
 */
function stage_file_purifier($dummy_files_path = NULL) {
  $files_path = stage_file_purifier_get_public_path();
  $total_size = 0;
  $extensions = array();
  $dummy_files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dummy_files_path), RecursiveIteratorIterator::SELF_FIRST);

  $ext_error = FALSE;
  $extensions_string = drush_get_option('extensions');
  if (!empty($extensions_string)) {
    // We have extensions in option, let's check samples file.
    $extensions = explode(',', $extensions_string);
    foreach ($extensions as $ext) {
      $ext = trim($ext);
      // Check if a sample file exists.
      $ext_exist = FALSE;
      foreach ($dummy_files as $dummy_file) {
        $sample_ext = $dummy_file->getExtension();
        if ($sample_ext == $ext) {
          $ext_exist = TRUE;
          break;
        }
      }
      if (FALSE == $ext_exist) {
        drush_log(dt('There is no sample file for the extension !ext', array('!ext' => $ext)), 'error');
        $ext_error = TRUE;
      }
    }
  }
  else {
    // No extensions as option, we store extensions based on sample files list.
    $extensions = array();
    foreach ($dummy_files as $dummy_file) {
      $ext = $dummy_file->getExtension();
      if (!empty($ext)) {
        $extensions[] = $ext;
      }
    }
  }

  // Exit on error or ask for user validation about extensions.
  if ($ext_error || !drush_confirm(dt('Are you sure you want to proceed to a file purification of following extensions: !exts', array('!exts' => implode(',', $extensions))))) {
    return;
  }

  $filetypes = array();
  foreach ($dummy_files as $dummy_file) {
    $ext = $dummy_file->getExtension();
    if (!empty($ext) && !$dummy_file->isLink() && in_array(strtolower($ext), $extensions)) {
      $dummy_path = stage_file_purifier_copy($dummy_file->getPathname());
      $filetypes[drupal_strtolower($ext)] = stage_file_purifier_realpath($dummy_path);
    }
  }

  $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($files_path), RecursiveIteratorIterator::SELF_FIRST);
  foreach ($objects as $object) {
    $ext = $object->getExtension();
    $file = $object->getPathname();
    $real = stage_file_purifier_realpath($file);
    if (!empty($ext) && !$object->isLink() && !in_array($real, $filetypes) && in_array(strtolower($ext), $extensions)) {
      if (isset($filetypes[drupal_strtolower($ext)])) {
        $size = filesize($file);
        if (is_int($size)) {
          $total_size += $size;
        }
        unlink($file);
        symlink($filetypes[drupal_strtolower($ext)], $real);
      }
    }
  }

  // Print total size.
  drush_log(dt('This execution of the file purifier saved !size bytes (!human_size)', array(
    '!size' => $total_size,
    '!human_size' => stage_file_purifier_human_filesize($total_size),
  )));
}

/**
 * Version-agnostic realpath.
 */
function stage_file_purifier_realpath($path) {
  if (!function_exists('drupal_realpath')) {
    return realpath($path);
  }
  else {
    return drupal_realpath($path);
  }
}

/**
 * Version-agnostic file copy.
 */
function stage_file_purifier_copy($source) {
  if (!function_exists('file_unmanaged_copy')) {
    file_copy($source, 0, FILE_EXISTS_REPLACE);
    return $source;
  }
  else {
    return file_unmanaged_copy($source, 'public://', FILE_EXISTS_REPLACE);
  }

}

/**
 * Retrieves the path of the files of Drupal regardless its version.
 */
function stage_file_purifier_get_public_path() {
  if (function_exists('file_directory_path')) {
    $files_path = file_directory_path();
  }
  else {
    $files_path = drupal_realpath('public://');
  }
  return $files_path;
}

/**
 * Get human-readable size from bytes size.
 */
function stage_file_purifier_human_filesize($bytes, $decimals = 2) {
  $sz = 'BKMGTP';
  $factor = floor((strlen($bytes) - 1) / 3);
  return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}
